import { Component, OnInit, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit,AfterViewInit,OnDestroy {
  public form = {
    email:null,
    password:null
    // user_type:null
  }
  signUpForm:FormGroup;
  constructor(private elementRef:ElementRef,private formBuilder: FormBuilder) { }

  ngOnInit() {
    // this.signUpForm = this.formBuilder.group({
    //   username: [''],
    //   email: ['',[Validators.required, Validators.email]],
    //   password: ['',[Validators.required]],
    //   confirmpassword: ['',[Validators.required]],
    //   legalname:['',Validators.required],
    //   gstin:['',Validators.required],
    //   is_client:['',Validators.required]
    // });
  }

  ngAfterViewInit(){
    console.log('Signup[After view init]')
    this.elementRef.nativeElement.ownerDocument.body.classList.add("banner-class");
  }
  ngOnDestroy(){
    if(this.elementRef.nativeElement.ownerDocument.body.classList.contains('banner-class')) {
      this.elementRef.nativeElement.ownerDocument.body.classList.remove("banner-class");
     
  }

  }
}
