import { Component, OnInit, AfterViewInit, ElementRef, OnDestroy } from '@angular/core';
import { EncryptiondecryptionService } from 'src/app/encryptiondecryption.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  
})
export class LoginComponent implements OnInit ,AfterViewInit,OnDestroy{
  // public form = {
  //   email:null,
  //   password:null
  //   // user_type:null
  // }
  loginForm:FormGroup;
  decryptedData = null;
  submitted = false;
  constructor(private elementRef:ElementRef,private encryptionDecryptionService:EncryptiondecryptionService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['',[Validators.required,Validators.email]],
      password: ['',[Validators.required, Validators.minLength(6)]],
    });
  }

  onSubmit(){
    this.submitted = true;
    console.log(this.loginForm.value);
    // console.log(this.encryptionDecryptionService.decryptData('TPXjgXUYZoD/tr5sh4vXRg=='));
      

  }

  get f() { return this.loginForm.controls; }


  ngAfterViewInit(){
    console.log('Login[After view init]',this.elementRef.nativeElement.ownerDocument.body)
    this.elementRef.nativeElement.ownerDocument.body.classList.add("banner-class");
  }
  ngOnDestroy(){
    if(this.elementRef.nativeElement.ownerDocument.body.classList.contains('banner-class')) {
      this.elementRef.nativeElement.ownerDocument.body.classList.remove("banner-class");
     
    }
    // console.log('Destroy[After view init]',this.elementRef.nativeElement.ownerDocument.body)
  }

}
