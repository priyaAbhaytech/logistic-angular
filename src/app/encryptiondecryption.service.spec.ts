import { TestBed, inject } from '@angular/core/testing';

import { EncryptiondecryptionService } from './encryptiondecryption.service';

describe('EncryptiondecryptionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EncryptiondecryptionService]
    });
  });

  it('should be created', inject([EncryptiondecryptionService], (service: EncryptiondecryptionService) => {
    expect(service).toBeTruthy();
  }));
});
